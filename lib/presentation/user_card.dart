import 'package:flutter/material.dart';
import 'package:novin_dev_test/data/model/user/data.dart';

class UserCard extends StatelessWidget {
  Data user;
  UserCard({Key key ,@required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("${user.firstName} ${user.lastName}"),
        automaticallyImplyLeading: true,
        titleSpacing: -2,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.symmetric(vertical: 20),
              child: CircleAvatar(
                radius: 80,
                backgroundImage:NetworkImage(user.avatar),),
            ),
            RichText(text: TextSpan(text: " Id : ",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24),children: [
              TextSpan(text: "${user.id}" ,style: TextStyle(color: Colors.blueGrey ,fontSize: 22))
            ])),
            SizedBox(height: 10,),
            RichText(text: TextSpan(text: "Name : ",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24),children: [
              TextSpan(text: "${user.firstName} ${user.lastName}" ,style: TextStyle(color: Colors.blueGrey ,fontSize: 22))
            ])),
            SizedBox(height: 10,),

            RichText(text: TextSpan(text: "Email : ",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 24),children: [
              TextSpan(text: "${user.email}" ,style: TextStyle(color: Colors.blueGrey ,fontSize: 20))
            ])),

          ],
        ),
      ),
    );
  }
}

import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:novin_dev_test/data/model/login/login_data_model.dart';
import 'package:novin_dev_test/data/model/user/data.dart';
import 'package:novin_dev_test/data/model/user/user_data_model.dart';
import 'package:novin_dev_test/presentation/user_card.dart';

class Users extends StatefulWidget {
  final int total;

  const Users({Key key, this.total}) : super(key: key);



  @override
  _UsersState createState() => _UsersState();
}

class _UsersState extends State<Users> {
  List<Data> users = [];
  List<Data> userList = [];

  @override
  void initState() {
    _getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return userList.isNotEmpty ?
      ListView.builder(
        itemCount: userList.length,
        physics: BouncingScrollPhysics(),
        itemBuilder: (BuildContext context, int position) {
          return
               Card(
                  child: Container(
                    child: ListTile(
                      title: RichText(
                          text: TextSpan(
                              text: "Name : ",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16),
                              children: [
                            TextSpan(
                                text:
                                    "${userList[position].firstName} ${userList[position].lastName}",
                                style: TextStyle(
                                    color: Colors.blueGrey, fontSize: 16))
                          ])),
                      subtitle: RichText(text: TextSpan(text: "email : ",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 16),children: [
                        TextSpan(text: "${userList[position].email}" ,style: TextStyle(color: Colors.blueGrey ,fontSize: 16))
                      ])),
                      leading: CircleAvatar(
                          radius: 30,
                          backgroundImage:NetworkImage(userList[position].avatar)),
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return UserCard(user: userList[position],);
                        }));
                      },
                    ),
                  ),
                );

        }):
    Center(
    child: CircularProgressIndicator(),
    );
  }

  void _getData() async {

    var url=Uri.parse("https://reqres.in/api/login");
    var response=await http.get(url) ;
      var jsonResponse=json.decode(response.body);
      var loginModel= LoginDataModel.fromJson(jsonResponse);

    for (int i = 1; i <= loginModel.total; i++) {
      var url = Uri.parse("https://reqres.in/api/users/$i");
      var response = await http.get(url);
      var jsonResponse = json.decode(response.body);
      var userModel = UserDataModel.fromJson(jsonResponse);
      users.add(userModel.data);
    }
    setState(() {
      userList.addAll(users);
    });
  }
  @override
  void dispose() {
    _getData();
    super.dispose();
  }
}

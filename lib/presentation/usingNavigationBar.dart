import 'package:flutter/material.dart';
import 'package:novin_dev_test/presentation/user_list.dart';
import 'login.dart';

class UsingNavigationBar extends StatefulWidget  {
  const UsingNavigationBar({Key key}) : super(key: key);

  @override
  _UsingNavigationBarState createState() => _UsingNavigationBarState();
}
TabController tabController;

class _UsingNavigationBarState extends State<UsingNavigationBar> with SingleTickerProviderStateMixin {
  @override
  void initState() {
    tabController = TabController(length: 2, vsync: this);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateRoute: (RouteSettings routeSettings) {
        switch (routeSettings.name) {
          case "login":
            return MaterialPageRoute(builder: (context) => Login());
          case "user":
            return MaterialPageRoute(builder: (context) => Users());

          default:
            return MaterialPageRoute(builder: (context) => Login());
        }
      },
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: TabBarView(
          controller: tabController,
          children: [
            Login(),
            Users(),
          ],
        ),
        appBar: AppBar(
          title: Text("NovinDev Test"),
          backgroundColor: Colors.lightBlue,
        ),
        bottomNavigationBar: ShowBottomNavigation(),
      ),
    );
}




}
class ShowBottomNavigation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      color: Colors.lightBlue,
      child: Container(
        height: 60,
        child: TabBar(
          controller:tabController ,
          indicatorColor: Colors.purpleAccent,
          unselectedLabelColor: Colors.white,
          labelColor: Colors.white70,

          tabs: [
            Tab(icon: Icon(Icons.login,),text: "Login",),

            Tab(icon: Icon(Icons.person,),text: "Users"),

          ],
        ),
      ),
    );
  }}
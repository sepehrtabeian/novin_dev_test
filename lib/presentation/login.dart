import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:http/http.dart' as http;
import 'package:novin_dev_test/data/model/login/data.dart';
import 'package:novin_dev_test/data/model/login/login_data_model.dart';
import 'package:novin_dev_test/presentation/user_card.dart';
import 'package:novin_dev_test/presentation/user_list.dart';
class Login extends StatefulWidget {
  const Login({Key key}) : super(key: key);
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  List<Data> logins=[];
  @override
  void initState() {
    _getData()  ;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

   return Scaffold(
      body: logins.isNotEmpty? Container(
        alignment: Alignment.center,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView.builder(
            itemCount: logins.length,
            physics: BouncingScrollPhysics(),
               itemBuilder: (BuildContext context,int position){
              return  Card(
                color: HexColor("${logins[position].color}"),
                // color:setColor(logins[position].color.replaceAll(new RegExp(r'[^\w\s]+'),''))
                  shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                 ),
                 elevation: 5,
                 child: Padding(
                   padding: const EdgeInsets.all(10.0),
                   child: Column(
                     mainAxisAlignment: MainAxisAlignment.center,
                     children: [
                       RichText(text: TextSpan(text: " Id : ",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 20),children: [
                         TextSpan(text: "${logins[position].id}" ,style: TextStyle(color: Colors.white ,fontSize: 20))
                       ])),

                       RichText(text: TextSpan(text: "Name : ",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 20),children: [
                         TextSpan(text: "${logins[position].name}" ,style: TextStyle(color: Colors.white ,fontSize: 20))
                       ])),

                       RichText(text: TextSpan(text: "Year : ",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 20),children: [
                         TextSpan(text: "${logins[position].year}" ,style: TextStyle(color: Colors.white ,fontSize: 20))
                       ])),
                       RichText(text: TextSpan(text: "color : ",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 22),children: [
                         TextSpan(text: "${logins[position].color}" ,style: TextStyle(color: Colors.white ,fontSize: 20))
                       ])),
                       RichText(text: TextSpan(text: "pantone_value : ",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 20),children: [
                         TextSpan(text: "${logins[position].pantoneValue}" ,style: TextStyle(color: Colors.white ,fontSize: 20))
                       ])),

                     ],

                   ),
                 ),
              );
               },

            ),
        ),
      ):Center(child: CircularProgressIndicator(),),
    );
  }

  void _getData() async{
    var url=Uri.parse("https://reqres.in/api/login");
    var response=await http.get(url) ;
    setState(() {
      var jsonResponse=json.decode(response.body);
     var loginModel= LoginDataModel.fromJson(jsonResponse);
      print(LoginDataModel.fromJson(jsonResponse).data.first.toString());
      loginModel.data.forEach((element) {
        logins.add(element);

      });


    });
  }
  @override
  void dispose() {
    _getData();
    super.dispose();
  }
}

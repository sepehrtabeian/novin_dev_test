/// id : 1
/// name : "cerulean"
/// year : 2000
/// color : "#98B2D1"
/// pantone_value : "15-4020"

class Data {
  int id;
  String name;
  int year;
  String color;
  String pantoneValue;

  Data({
      this.id, 
      this.name, 
      this.year, 
      this.color, 
      this.pantoneValue});

  Data.fromJson(dynamic json) {
    id = json["id"];
    name = json["name"];
    year = json["year"];
    color = json["color"];
    pantoneValue = json["pantone_value"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = id;
    map["name"] = name;
    map["year"] = year;
    map["color"] = color;
    map["pantone_value"] = pantoneValue;
    return map;
  }

}
/// url : "https://reqres.in/#support-heading"
/// text : "To keep ReqRes free, contributions towards server costs are appreciated!"

class Support {
  String url;
  String text;

  Support({
      this.url, 
      this.text});

  Support.fromJson(dynamic json) {
    url = json["url"];
    text = json["text"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["url"] = url;
    map["text"] = text;
    return map;
  }

}